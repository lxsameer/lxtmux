#!/usr/bin/env bash

git_branch() {
    git branch |grep "*"|cut -d ' ' -f 2
}


main() {
    br=$(git_branch)
    echo "$@"
}

main
