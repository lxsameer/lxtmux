#!/usr/bin/env bash

running_containers() {
    docker ps -q|wc -l
}

all_containers() {
    docker ps -a -q|wc -l
}


main() {
    echo "$(running_containers) | $(all_containers)"
}

main
