#!/usr/bin/env bash

# This is a clone from dracula
# source and run lxtmux theme

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

$current_dir/scripts/lxtmux.sh

